# C++ Program: Binary Tree


## Program Details
### Purpose
Practice with **Binary Trees**.

Specifically, this program deals with ***Binary Search Trees***. Both balanced and unbalanced trees are possible upon generating nodes. Trees are populated with nodes of different ***id*** values ranging "100-999". The number of nodes present inside the tree are determined by the command line argument provided of the range "1-10".

The entire binary tree data structure is built as a class. Encapsulated member functions enable operations such as checking tree height, checking node count, creating nodes, deleting nodes, searching nodes, in-order traversal, pre-order traversal, post-order traversal, deleting the tree, and printing the tree.

IMPORATN NOTE: Its recommended to provide a command line argument no bigger than "7", so that built trees will fit on the screen. Given how command terminals are, they wrap text onto a new line if they think its to long. Physically small screens will likely see warped trees even at "7" nodes. Make sure to expand the terminal window as wide as possible.

### Relevant Data Structures, Algorithms, or Library Functions
- The C++ library function **rand()** is used to generate random values to populate a Binary Tree instance.
In this instance, random numbers created are ones from "100-999". Such is the possible range of any tree node's possible ***id*** value.

- Code is seen separated accross different files. This **Modular Programming** methodology is used in a professional sense no matter which programming language is employed. This small program is just a simple opportunity to practice.

- **Object-Oriented Progamming**. The ***BinaryTree*** *class* structure encapsulates public/private methods and data structures which relate to the building and modification of a binary tree. The ***TreeNode*** *struct* describes the shape of a single tree node, and it makes multiple instances within one ***BinaryTree*** instance. The constructors for both mentioned structures are *overloaded* to allow for the initialization of objects with specific information. Also noteworthy, is the inherent existence of **Pointers** that both connect and help dynamically modify the overall structure.  

- The **Queue** data structure was applied in 2 ***BinaryTree*** methods. Inside node deletion and in a print function. Inside the node deletion method, a queue is used to update the levels of any children nodes that were attached to the deleted node. For the method to print the binary tree instance, a queue offers a way to print tree contents by level.   

- Most private methods within ***BinaryTree*** are recursive. **Recursion** was chosen for its natural and straightforward manner of tree traversal and tree modification. Although functional, it's not most optimal to use recursion for trees, but it was applied here to see and learn from.

- Performance metrics:
  
    - **Time Complexity**. How does input influence time?
  
        Adding a new node, deleting a node, or searching a node are operations of the same time complexity. The amount of time taken by an imbalanced tree is largely affected by height ***h***. In the absolute worst case scenario(*the tree becomes a linked list*) nodes ***n*** will equal ***h***, so a ***Linear*** time complexity is more apparent.

        <br/>

        ![BigOSs](images/tree_bigOspace.png)

        <br/>

        On the other hand, the best case scenario happens while a balanced tree exists. The recursive call stack depth, capped at ***h***, implies that the number of movements needed for an operation would not have to visit every single node. This reduction in traversal contributes to a decrease in the overall time required for the operation. One can see that ***n*** and ***h*** are related by:

        <br/>
        
        ![NodeFormula](images/tree_nodeFormula.png)

        <br/>
        
        With log properties, the formula above can be solved for ***h*** and simplifies to:

        <br/>
        
        ![NodeFormulaSimplified](images/tree_SimplifNodForm.png)

        <br/>
        
        All in all, the trend seen for time complexity is ***Logarithmic***.

        <br/>
        
        ![NodeFormula](images/tree_bigOspace2.png)

    - **Space Complexity**. How does input influence space?
        
        Potential space taken up by this tree structure is limited to the number of tree nodes ***n*** present. Being so, this leads to a ***Linear*** space complexity.

        <br/>
        
        ![BigoTsb](images/tree_bigOtime.png)

        <br/>

## Program Installation
Before working with C++, it’s essential to set up the correct environment. One can simply obtain it out of the box with
certain IDE's such as **CLion**, **Eclipse**, or **Code::Blocks**. Others such as **Visual Studio**,
may require pluggins to handle "C/C++". The chosen option for the C++ program shown here were the
**Visual Studio** puggins. The following link has directions on how to achive this: 
https://learn.microsoft.com/en-us/cpp/build/vscpp-step-0-installation?view=msvc-170

With the C++ compiler installed, a simple copy and paste of the entire `.cpp` program onto a brand new
personal `.cpp` file would be the quickest way to run this program. Save the new file. Push the "Run" button
and done. Otherwise, a command terminal can be opened where the personal `.cpp` file exists, and the 
following commands can be entered to run the application:
```console
g++ -o  executable myFileNameHere.cpp
```
```console
.\executable 7
```

## Use Case/Example Output
```c++
-------------------------------------------------------------------------------------
|                                TEST: The Empty Tree                               |
-------------------------------------------------------------------------------------
Empty Tree?: Yes
Tree Height: 0
Node Count:  0
Pre Order Traversal: The tree is empty.
In Order Traversal:  The tree is empty.
Post Order Traversal:The tree is empty.
Visual of Binary Tree: Tree does not exist

***ACTION 1: Remove non-existing ID value "999".
***Success should equal "No": No

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: Yes
Tree Height: 0
Node Count:  0
Pre Order Traversal: The tree is empty.
In Order Traversal:  The tree is empty.
Post Order Traversal:The tree is empty.
Visual of Binary Tree: Tree does not exist

***ACTION 2: Search for non-existing ID value "854".
***Success should equal "No": No

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: Yes
Tree Height: 0
Node Count:  0
Pre Order Traversal: The tree is empty.
In Order Traversal:  The tree is empty.
Post Order Traversal:The tree is empty.
Visual of Binary Tree: Tree does not exist

***ACTION 3: Deleting an empty tree.
***Success should equal "No": No

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: Yes
Tree Height: 0
Node Count:  0
Pre Order Traversal: The tree is empty.
In Order Traversal:  The tree is empty.
Post Order Traversal:The tree is empty.
Visual of Binary Tree: Tree does not exist



-------------------------------------------------------------------------------------
|                               TEST: The Filled Tree                               |
-------------------------------------------------------------------------------------
...adding nodes equal to given argument. Success should equal "Yes": Yes
Empty Tree?: No
Tree Height: 5
Node Count:  7
Pre Order Traversal: 635  311  275  561  453  423  740
In Order Traversal:  275  311  423  453  561  635  740
Post Order Traversal:275  423  453  561  311  740  635
Visual of Binary Tree:
Lv 1:                                                                                (635)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 2:                                        (311)                                                                           (740)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 3:                    (275)                                   (561)                                   null                                    null 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 4:          null                null                (453)               null                null                null                null                null      
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 5:     null      null      null      null      (423)     null      null      null      null      null      null      null      null      null      null      null 

***ACTION 1: Search for non-existing ID value "50".
***Success should equal "No": No

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: No
Tree Height: 5
Node Count:  7
Pre Order Traversal: 635  311  275  561  453  423  740
In Order Traversal:  275  311  423  453  561  635  740
Post Order Traversal:275  423  453  561  311  740  635
Visual of Binary Tree:
Lv 1:                                                                                (635)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 2:                                        (311)                                                                           (740)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 3:                    (275)                                   (561)                                   null                                    null 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 4:          null                null                (453)               null                null                null                null                null      
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 5:     null      null      null      null      (423)     null      null      null      null      null      null      null      null      null      null      null 

***ACTION 2: Search for "561".
***Success?: Yes

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: No
Tree Height: 5
Node Count:  7
Pre Order Traversal: 635  311  275  561  453  423  740
In Order Traversal:  275  311  423  453  561  635  740
Post Order Traversal:275  423  453  561  311  740  635
Visual of Binary Tree:
Lv 1:                                                                                (635)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 2:                                        (311)                                                                           (740)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 3:                    (275)                                   (561)                                   null                                    null
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 4:          null                null                (453)               null                null                null                null                null 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 5:     null      null      null      null      (423)     null      null      null      null      null      null      null      null      null      null      null 

***ACTION 3: Delete value "561".
***Success?: Yes

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: No
Tree Height: 5
Node Count:  6
Pre Order Traversal: 635  311  275  453  423  740
In Order Traversal:  275  311  423  453  635  740
Post Order Traversal:275  423  453  311  740  635
Visual of Binary Tree:
Lv 1:                                                                                (635)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 2:                                        (311)                                                                           (740)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 3:                    (275)                                   (453)                                   null                                    null
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 4:          null                null                (423)               null                null                null                null                null 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 5:     null      null      null      null      null      null      null      null      null      null      null      null      null      null      null      null 

***ACTION 4: Delete non-existing ID value "561".
***Success should equal "No": No

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: No
Tree Height: 5
Node Count:  6
Pre Order Traversal: 635  311  275  453  423  740
In Order Traversal:  275  311  423  453  635  740
Post Order Traversal:275  423  453  311  740  635
Visual of Binary Tree:
Lv 1:                                                                                (635)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 2:                                        (311)                                                                           (740)
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 3:                    (275)                                   (453)                                   null                                    null
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 4:          null                null                (423)               null                null                null                null                null 
---------------------------------------------------------------------------------------------------------------------------------------------------------------------
Lv 5:     null      null      null      null      null      null      null      null      null      null      null      null      null      null      null      null 

***ACTION 5: Delete entire tree.
***Success?: Yes

                                 Data After Action
                             - - - - - - - - - - - - -
Empty Tree?: Yes
Tree Height: 0
Node Count:  0
Pre Order Traversal: The tree is empty.
In Order Traversal:  The tree is empty.
Post Order Traversal:The tree is empty.
Visual of Binary Tree: Tree does not exist
```