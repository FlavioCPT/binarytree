/* ***********************************************************************************************
 * Name: Flavio C. Palacios Tinoco                                                               *
 * Assignment: 03                                                                                *
 * Purpose: Additional functions.                                                                *
 * Notes: Functions defined here are only called by "main.cpp". One function here is used to help*
 *        test validity of the given command line arguments. The other is to prevent repetition. * 
 * ***********************************************************************************************/
#include "main.h"

//- - - - - - - - - - - - - - - - - -
//     ***INPUT VALIDATION***
//- - - - - - - - - - - - - - - - - -
bool checkArgs(int argC,char *argV){
    //One bool is to check appropriate argument count and
    //the other is for ensuring proper "char" values.
    bool validArgsNumb=true;
    bool charsAreDigits=true;

    //No more than 2 arguments may be passed. The project ".exe" name
    //and the digit in "char" form are enough.
    if((argC>2)||(argC<2)){
        cout<<"ERROR: Too many or no arguments given. Pass no more than one."<<endl;
        validArgsNumb=false;
    }
    else{
        //Iteration through the "value" array is done and every
        //element is checked to make sure it's not a letter using
        //the "isdigit()" function.
        const int LENGTH=strlen(argV);
        int iteration=0;
        while (iteration!=LENGTH){
            if (!isdigit(*(argV+iteration))){
                charsAreDigits=false;
            };
            iteration++;
        };

        //Should the "charsAreDigits" flag still not be changed,
        //then the value is converted into an "int" using the
        //"atoi()" function.
        if(charsAreDigits==true){
            int intValue;
            intValue=atoi(argV);
            if((intValue<1)||(intValue>10)){charsAreDigits=false;};
        };
    };

    //Both bools are checked in order to return the appropriate result.
    if((validArgsNumb==true)&&(charsAreDigits==true)){return true;}
    else{
        if(charsAreDigits==false){
            cout<<"ERROR: Only number values between \"1\" and \"10\" are acceptable."<<endl;
        };
        return false;
    };
}

//- - - - - - - - - - - - - -
//   ***TRANSFORM A BOOL***
//- - - - - - - - - - - - - -
string successEcho(bool success){
    //A simple code snippet used various times in "main.cpp". 
    string successString;
    if(success==true){successString="Yes";}
    else{successString="No";};
    return successString;
}

//- - - - - - - - - - - - - - - - - -
//   ***SHOWCASE TREE FUNCTIONS***
//- - - - - - - - - - - - - - - - - -
void printTreeData(BinaryTree Tree){
    bool success; //Used with "successEcho()".
    int h; //Tree height.
    int n; //Tree nodes number.

    cout<<setw(85)<<left<<"                                 Data After Action                               "<<endl;
    cout<<setw(85)<<left<<"                             - - - - - - - - - - - - -                           "<<endl;
    success=Tree.isEmpty();
    h=Tree.getTreeHeight();
    n=Tree.getNumberOfNodes();
    cout<<"Empty Tree?: "<<successEcho(success)<<endl;
    cout<<"Tree Height: "<<h<<endl;
    cout<<"Node Count:  "<<n<<endl;
    cout<<"Pre Order Traversal: ";
    Tree.preorderTraverse();
    cout<<endl;
    cout<<"In Order Traversal:  ";
    Tree.inorderTraverse();
    cout<<endl;
    cout<<"Post Order Traversal:";
    Tree.postorderTraverse();
    cout<<endl;
    cout<<"Visual of Binary Tree: ";
    Tree.printLeavesAndEdges();
    cout<<endl<<endl;
}