/* ***********************************************************************************************
 * Name: Flavio C. Palacios Tinoco                                                               *
 * Assignment: 03                                                                                *
 * Purpose: Binary tree class functions definitions.                                             *
 * Notes: "binaryTree.h" class function defitions are found here. Private functions are listed   *
 *        first, then the Public functions are listed. This "binaryTree.cpp" file is included in *
 *        the "main.cpp" header.                                                                 *
 * ***********************************************************************************************/
#include "binaryTree.h"

//- - - - - - - - - - - - 
//   ***CONSTRUCTOR***
//- - - - - - - - - - - - 
BinaryTree::BinaryTree(){
    treeHeight=0;
    nodeCount=0;
    root=nullptr;
}

//- - - - - - - - - - - - 
//   ***CONSTRUCTOR***
//- - - - - - - - - - - - 
TreeNode::TreeNode(int givenId,int givenLevelValue){
    id=givenId;
    nodeLevel=givenLevelValue;
    leftChild=nullptr;
    rightChild=nullptr;
}

//- - - - - - - - - - - - -
//   ***(1) ADD A NODE***
//- - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::addHelper(struct TreeNode *&createNodePtr,struct TreeNode *tempPtr,int randomId){
    //If an empty tree, create the first node and increase "nodeCount" by 1.
    if(root==nullptr){  
        root=new TreeNode(randomId,1);
        
        nodeCount++;
        treeHeight++;
    }

    //Here, a node is created when at least one node is already in existence, and
    //when the "createNodePtr" is set to null because of the recursive call to the
    //"addHelper()" function.
    else if(createNodePtr==nullptr){
        createNodePtr=new TreeNode(randomId,(tempPtr->nodeLevel+1));
        nodeCount++;
        if(treeHeight<createNodePtr->nodeLevel){treeHeight++;};
    }

    //Here the "createNodePtr" is taken down recursively down the left side of the tree until
    //it becomes a "leftChild" pointer that points to NULL.
    else if(randomId<createNodePtr->id){
        tempPtr=createNodePtr;
        addHelper(createNodePtr->leftChild,tempPtr,randomId);
    }

    //Similar to how the "createNodePtr" travels down to the next left node, the same happens for traveling
    //down to a rightmost node. So long as the "randomId" value is more than the current node "createNodePtr"
    //points to, then the "addHelper()" function is recursively called.
    else if(randomId>createNodePtr->id){
        tempPtr=createNodePtr;
        addHelper(createNodePtr->rightChild,createNodePtr,randomId);
    };
}

//- - - - - - - - - - - - - - - - - - -
//   ***(2) DELETE THE ENTIRE TREE***
//- - - - - - - - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::clearHelper(struct TreeNode *clearPtr){
    //With every recursive call, the "root" pointer is the one traversing every node
    //until everyone is gone.
    root=clearPtr;

    //The "root" will eventually equal NULL. When this happens, the program terminates
    //this recursive instance of "clearHelper()"'s function call by doing nothing, and then, the
    //The program reverts to the previous recursive call of "clearHelper()". Once, "root"
    //has attempted to go left then right within every node, the node "root" points to is
    //deleted and the node counter decrements by 1 per deletion.
    if(clearPtr==nullptr){
        //Do Nothing.
    }
    else{
        clearHelper(clearPtr->leftChild);
        clearHelper(clearPtr->rightChild);

        delete clearPtr;
        nodeCount--;
    };

    treeHeight=0;
}

// - - - - - - - - - - - - - - - - - -
//   ***(3) PRINTING THE TREE IDs***
// - - - - - - - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::preorderTraverseHelper(struct TreeNode *traverserPtr){
    if(root==nullptr){
        cout<<"The tree is empty.";
    }
    else{
        cout<<traverserPtr->id<< "  ";
        if(traverserPtr->leftChild!=nullptr){
            preorderTraverseHelper(traverserPtr->leftChild);
        };
        if(traverserPtr->rightChild!=nullptr){
            preorderTraverseHelper(traverserPtr->rightChild);
        };
    };
}

// - - - - - - - - - - - - - - - - - -
//   ***(4) PRINTING THE TREE IDs***
// - - - - - - - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::inorderTraverseHelper(struct TreeNode *traverserPtr){
    if(root==nullptr){
        cout<<"The tree is empty.";
    }
    else{
        //Here the "inorderTraverseHelper()" function is called recusively
        //to go leftward on the binary tree every time this function is called.
        //As it does so, every previous function call still has an old copy of
        //the node "traversesrPtr" used to point to, and every previous function
        //call waits until every future function call has returned so that it may
        //then return.
        if(traverserPtr->leftChild!=nullptr){
            inorderTraverseHelper(traverserPtr->leftChild);
        };

        //If "traverserPtr" cannot go left anymore then it prints the value held
        //in the current node.
        cout<<traverserPtr->id<<"  ";

        //Once going left is no longer possible, "traverserPtr" goes right.
        if(traverserPtr->rightChild!=nullptr){
            inorderTraverseHelper(traverserPtr->rightChild);
        };
    };
}

// - - - - - - - - - - - - - - - - - -
//   ***(5) PRINTING THE TREE IDs***
// - - - - - - - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::postorderTraverseHelper(struct TreeNode *traverserPtr){
    if(root==nullptr){
        cout<<"The tree is empty.";
    }
    else{
        if(traverserPtr->leftChild!=nullptr){
            postorderTraverseHelper(traverserPtr->leftChild);
        };
        if(traverserPtr->rightChild!=nullptr){
            postorderTraverseHelper(traverserPtr->rightChild);
        };
        cout<<traverserPtr->id<<"  ";
    };
}

//- - - - - - - - - - - - - - - - - - - - - - - -
//     ***(6) PSEUDORANDOM NUMBER GENERATOR***
//- - - - - - - - - - - - - - - - - - - - - - - -
//--PRIVATE FUNCTION--
void BinaryTree::pseudoRandIntGenerator(const int ARRAY_SIZE){
    //The "pseudoRandomInt" generated will be stored in the "randomIntArray".
    int randomIntArray[ARRAY_SIZE];
    int pseudoRandInt;
    srand(time(NULL));

    //The array size will be determined by the number that the user has passed
    //in from  the command line. Therefore, the following "for" loop will iterate
    //an equal amount to that number. Random numbers are generated in range "100-999".
    int secondIndex;
    for(int index=0;index<ARRAY_SIZE;index++){
        pseudoRandInt=(rand()%900)+100;

        //This inner "while" loop will ensure that every random number in the array
        //is unique. At every "for" loop iteration, this inner loop will compare the
        //generated random number to every previous index in the array, and if a copy
        //is found, another random number is generated. The only way to exit this "while"
        //loop is to make a unique number.
        index++;
        secondIndex=0;
        while(secondIndex!=index){
            if(pseudoRandInt==randomIntArray[secondIndex]){
                pseudoRandInt=(rand()%900)+100;
                secondIndex=0;
            };
            secondIndex++;
        };
        index--;

        //The generated unique number is now assigned to an index in the array.
        *(randomIntArray+index)=pseudoRandInt;
    };

    //Now, every random number in the array is passed on to the "addHelper()" function.
    for (int index=0;index<ARRAY_SIZE;++index){
        pseudoRandInt=*(randomIntArray+index);
        struct TreeNode *createNodePtr=root;
        struct TreeNode *tempPtr=root;
        addHelper(createNodePtr,tempPtr,pseudoRandInt);  //Private class function (#1).
    };
}

//- - - - - - - - - - - - - - - -
//   ***IS THE TREE EMPTY?***
//- - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
bool BinaryTree::isEmpty(){
    if(root==nullptr){return true;}
    else{return false;}
}

//- - - - - - - - - - - - - - - - -
//   ***TREE HEIGHT ACCESSOR***
//- - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
int BinaryTree::getTreeHeight(){
    return treeHeight;
}

//- - - - - - - - - - - - - - - -
//   ***NODE COUNT ACCESSOR***
//- - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
int BinaryTree::getNumberOfNodes(){
    return nodeCount;
}

//- - - - - - - - - - - - - - - - - - - - - - - -
//   ***CALL PRIVATE NODE CREATION FUNCTION***
//- - - - - - - - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
bool BinaryTree::addNode(int givenNumber){  //Calls private function (#7) which then calls recursive (#2).
    pseudoRandIntGenerator(givenNumber);
    return true;
}

//- - - - - - - - - - - - -
//   ***REMOVE A NODE***
//- - - - - - - - - - - - -
//--PUBLIC FUNCTION--
bool BinaryTree::remove(int givenId){
        //"tempPtr" is the temporary pointer used to prepare node connections before a node
        //has been deleted. "deletionPtr" will always point to the node destined for deletion.
        //"temPtr" will be the parent of "deletionPtr". If "tempPtr" shall equal
        //"deletionPtr", then we know that the root node is about to be deleted.
        struct TreeNode *deletionPtr=root;
        struct TreeNode *tempPtr=root;

        //A binary search is done in order to see if the "givenId" exists. After this loop,
        //if "deletionPtr" happens to equal NULL, this means there was never a tree to begin with,
        //or the "givenId" value does not exist in the tree. Therefore, we go no further and return
        //"false".
        while((deletionPtr!=nullptr)&&(deletionPtr->id!=givenId)){
            tempPtr=deletionPtr;
            if(deletionPtr->id>givenId){
                deletionPtr=deletionPtr->leftChild;
            }
            else{
                deletionPtr=deletionPtr->rightChild;
            };
        };
        if(deletionPtr==nullptr){return false;}
        //Case1: Node about to be deleted has no children. The parent node for this node has its connecting
        //pointer set to NULL.
        if((deletionPtr->leftChild==nullptr)&&(deletionPtr->rightChild==nullptr)){
            if(deletionPtr==tempPtr){
                //Do nothing. Exit this "if" statement and have the root node deleted.
            }
            else if(tempPtr->id>deletionPtr->id){
                tempPtr->leftChild=nullptr;
            }
            else{
                tempPtr->rightChild=nullptr;
            };
        }

        //Case2: Node about to be deleted has one child to the right. Simply put, the only child had by the
        //node about to be deleted is now pointed to by the parent of the deleted node.
        else if((deletionPtr->leftChild==nullptr)&&(deletionPtr->rightChild!=nullptr)){
            queue <struct TreeNode*> needLevelUpdate;  //Queue used to update node levels.

            if(deletionPtr==tempPtr){
                root=deletionPtr->rightChild;
            }
            else if(tempPtr->id>deletionPtr->id){
                tempPtr->leftChild=deletionPtr->rightChild;
            }
            else{
                tempPtr->rightChild=deletionPtr->rightChild;
            };

            traverserPtr=deletionPtr->rightChild;
            needLevelUpdate.push(traverserPtr);
            while(!needLevelUpdate.empty()){  //Queue cycles through "deletionPtr's" sub-tree.
                traverserPtr=needLevelUpdate.front();
                needLevelUpdate.pop();

                if(traverserPtr->leftChild!=nullptr){needLevelUpdate.push(traverserPtr->leftChild);};
                if(traverserPtr->rightChild!=nullptr){needLevelUpdate.push(traverserPtr->rightChild);};
                
                traverserPtr->nodeLevel=traverserPtr->nodeLevel-1;  //Sub-tree's node levels are updated.
            };
        }

        //Case3: Node about to be deleted has one child to the left. Similar to Case 2, the only difference is the
        //connection will happen on the parent's left.
        else if((deletionPtr->leftChild!=nullptr)&&(deletionPtr->rightChild==nullptr)){
            queue <struct TreeNode*> needLevelUpdate;  //Queue used to update node levels.

            if(deletionPtr==tempPtr){
                root=deletionPtr->leftChild;
            }
            else if(tempPtr->id>deletionPtr->id){
                tempPtr->leftChild=deletionPtr->leftChild;
            }
            else{
                tempPtr->rightChild=deletionPtr->leftChild;
            };

            traverserPtr=deletionPtr->leftChild;
            needLevelUpdate.push(traverserPtr);
            while(!needLevelUpdate.empty()){  //Queue cycles through "deletionPtr's" sub-tree.
                traverserPtr=needLevelUpdate.front();
                needLevelUpdate.pop();

                if(traverserPtr->leftChild!=nullptr){needLevelUpdate.push(traverserPtr->leftChild);};
                if(traverserPtr->rightChild!=nullptr){needLevelUpdate.push(traverserPtr->rightChild);};
                
                traverserPtr->nodeLevel=traverserPtr->nodeLevel-1;  //Sub-tree's node levels are updated.
            };
        }

        //Case4: Node about to be deleted has two children. A new node will replace the deleted node. That
        //node is the smallest value in "deletionPtr's" right child sub-tree.
        else{
            //"replacementNodePtr" will identify the smallest value in "deletionPtr's" right child sub-tree.
            //"secondTempPtr" helps with fixing the node connections before a node deletion, and it is always
            //going to be "replacementNodePtr's" parent node (unless the root node is the node marked for deletion).
            struct TreeNode *replacementNodePtr=deletionPtr;
            replacementNodePtr=replacementNodePtr->rightChild;
            struct TreeNode *secondTempPtr=replacementNodePtr;
            queue <struct TreeNode*> needLevelUpdate;  //Queue used to update node levels.

            while (replacementNodePtr->leftChild!=nullptr){
                secondTempPtr=replacementNodePtr;
                replacementNodePtr=replacementNodePtr->leftChild;
            };

            if(deletionPtr==tempPtr){
                //At this point here: "deletionPtr", "tempPtr", and "root" are the same.
                root=replacementNodePtr;
            }
            else if(tempPtr->id>replacementNodePtr->id){
                tempPtr->leftChild=replacementNodePtr;
            }
            else{
                tempPtr->rightChild=replacementNodePtr;
            };

            if(secondTempPtr==replacementNodePtr){
                //New root. and its new connections.
                replacementNodePtr->leftChild=deletionPtr->leftChild;
                replacementNodePtr->rightChild=nullptr;
            }else{
                replacementNodePtr->leftChild=deletionPtr->leftChild;
                replacementNodePtr->rightChild=deletionPtr->rightChild;
                secondTempPtr->leftChild=nullptr;
            };

            traverserPtr=replacementNodePtr;
            needLevelUpdate.push(traverserPtr);
            while(!needLevelUpdate.empty()){  //Queue cycles through "replacementNodePtr's" sub-tree.
                traverserPtr=needLevelUpdate.front();
                needLevelUpdate.pop();

                if(traverserPtr->leftChild!=nullptr){needLevelUpdate.push(traverserPtr->leftChild);};
                if(traverserPtr->rightChild!=nullptr){needLevelUpdate.push(traverserPtr->rightChild);};
                
                traverserPtr->nodeLevel=traverserPtr->nodeLevel-1;  //Sub-tree's node levels are updated.
            };
        };

        //Making it this far means the "givenId" actually existed in the tree, was properly isolated
        //from the tree. The targeted node can safely be deleted.
        tempPtr=nullptr;
        traverserPtr=nullptr;
        nodeCount--;
        delete deletionPtr;
        return true;
}

//- - - - - - - - - - - - - - - - - - - -
//   ***CALL PRIVATE CLEAR FUNCTION***
//- - - - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
bool BinaryTree::clear(){  //Calls recursive private function (#2).
    traverserPtr=root;
    
    if(root==nullptr){return false;}
    else{
        clearHelper(traverserPtr);
        return true;
    };
};

//- - - - - - - - - - - - - - - - - -
//   ***SEARCH FOR A DESIRED ID***
//- - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
bool BinaryTree::contains(int desiredId){
    bool success;
    traverserPtr=root;
    if(traverserPtr==nullptr){
        success=false;
    }
    else{
        //Binary search is done to try and find a match for the
        //wanted "id".
        while((traverserPtr!=nullptr)&&(traverserPtr->id!=desiredId)){
            if(traverserPtr->id>desiredId){traverserPtr=traverserPtr->leftChild;}
            else{traverserPtr=traverserPtr->rightChild;};
        };

        if(traverserPtr!=nullptr){success=true;}
        else{success=false;};
    };

    traverserPtr=nullptr;
    return success;
}

//- - - - - - - - - - - - - - - - - -
//   ***CALL PREORDER TRAVERSAL***
//- - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
void BinaryTree::preorderTraverse(){  //Calls recursive private function (#3).
    traverserPtr=root;  
    preorderTraverseHelper(traverserPtr);  
};

//- - - - - - - - - - - - - - - - - -
//   ***CALL INORDER TRAVERSAL***
//- - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
void BinaryTree::inorderTraverse(){  //Calls recursive private function (#4).
    traverserPtr=root;
    inorderTraverseHelper(traverserPtr);
};

//- - - - - - - - - - - - - - - - - -
//   ***CALL POSTORDER TRAVERSAL***
//- - - - - - - - - - - - - - - - - -
//--PUBLIC FUNCTION--
void BinaryTree::postorderTraverse(){  //Calls recursive private function (#5).
    traverserPtr=root;
    postorderTraverseHelper(traverserPtr);
};

void BinaryTree::printLeavesAndEdges(){
    if(root==nullptr){
        cout<<"Tree does not exist";
    }
    else{
        int childLorR;  //Left child="0" and Right child="1".
        int prevNodeLevel;
        int treeHeightCopy;  //Helps "nodeSpacingVal".
        int nodeSpacingVal;  //Node spacing depending on tree level.
        string blankSpaceStr;  //Holds " ".
        string levelSeparatorStr;  //Holds "-".

        struct TreeNode *emptyNode=new TreeNode(0,0);  //Blank node created.
        queue <struct TreeNode *> visitedNodes;  //Dynamic queue. Helps print the tree. 
        childLorR=0;
        blankSpaceStr.reserve(5);  
        levelSeparatorStr.reserve(5+(pow(2,(treeHeight-1))*5*2));

        blankSpaceStr=string(5,' ');  //Prepare blank prints and traversal pointer.
        levelSeparatorStr=string(5+(pow(2,(treeHeight-1))*5*2),'-');
        treeHeightCopy=treeHeight;
        nodeSpacingVal=pow(2,treeHeightCopy-1);
        traverserPtr=root;
        
        visitedNodes.push(traverserPtr);  //Enqueue first tree node.
        prevNodeLevel=traverserPtr->nodeLevel; 

        while(!visitedNodes.empty()){  //Process the queue until empty.
            traverserPtr=visitedNodes.front();  //Before deletion, point to the first node in line.
            visitedNodes.pop();

            if(traverserPtr->leftChild!=nullptr){visitedNodes.push(traverserPtr->leftChild);}  //Enqueue children nodes.
            else if((traverserPtr->leftChild==nullptr)&&(traverserPtr->id!=0)&&(traverserPtr->nodeLevel<treeHeight)){
                visitedNodes.push(emptyNode=new TreeNode(0,traverserPtr->nodeLevel+1));
            }
            else if((traverserPtr->leftChild==nullptr)&&(traverserPtr->id==0)&&(traverserPtr->nodeLevel<treeHeight)){
                visitedNodes.push(emptyNode=new TreeNode(0,traverserPtr->nodeLevel+1));
            };
            if(traverserPtr->rightChild!=nullptr){visitedNodes.push(traverserPtr->rightChild);}
            else if((traverserPtr->rightChild==nullptr)&&(traverserPtr->id!=0)&&(traverserPtr->nodeLevel<treeHeight)){
                visitedNodes.push(emptyNode=new TreeNode(0,traverserPtr->nodeLevel+1));
            }
            else if((traverserPtr->rightChild==nullptr)&&(traverserPtr->id==0)&&(traverserPtr->nodeLevel<treeHeight)){
                visitedNodes.push(emptyNode=new TreeNode(0,traverserPtr->nodeLevel+1));
            };
                                                                                 
            if(traverserPtr==root){  //Print root node.
                cout<<endl<<"Lv "<<traverserPtr->nodeLevel<<":";
                for(int i=0;i<nodeSpacingVal;i++){cout<<blankSpaceStr;};
                cout<<"("<<to_string(traverserPtr->id)<<")";
            }
            else{  //Print non-root node(at newline).
                if(traverserPtr->nodeLevel>prevNodeLevel){ 
                    treeHeightCopy--;
                    nodeSpacingVal=pow(2,treeHeightCopy-1);
                    cout<<endl<<levelSeparatorStr;

                    if((traverserPtr->id==0)&&(traverserPtr->nodeLevel<=treeHeight)&&(childLorR==0)){
                        cout<<endl<<"Lv "<<traverserPtr->nodeLevel<<":";
                        for(int i=0;i<nodeSpacingVal;i++){cout<<blankSpaceStr;};
                        cout<<"null ";
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                    }
                    else if((traverserPtr->id!=0)&&(traverserPtr->nodeLevel<=treeHeight)&&(childLorR==0)){
                        cout<<endl<<"Lv "<<traverserPtr->nodeLevel<<":";
                        for(int i=0;i<nodeSpacingVal;i++){cout<<blankSpaceStr;};
                        cout<<"("<<to_string(traverserPtr->id)<<")";
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                    };
                }
                else{  //Print non-root node(not at newline).
                    if((traverserPtr->id==0)&&(childLorR==0)){
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                        cout<<"null ";
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                    }
                    else if((traverserPtr->id==0)&&(childLorR==1)){
                        cout<<"null ";
                    }
                    else if((traverserPtr->id!=0)&&(childLorR==0)){
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                        cout<<"("<<to_string(traverserPtr->id)<<")";
                        for(int i=0;i<(nodeSpacingVal*2-1);i++){cout<<blankSpaceStr;};
                    }
                    else{
                        cout<<"("<<to_string(traverserPtr->id)<<")";
                    };
                };
                
                if(childLorR==0){childLorR=1;}  //Update left/right flag.
                else{childLorR=0;};
            };                                                                                                                                
            prevNodeLevel=traverserPtr->nodeLevel;  //Update previous node level.
        }

        delete emptyNode;  //Delete and manage necessary pointers.
        traverserPtr=root;
    };
}