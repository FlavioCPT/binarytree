/* ***********************************************************************************************
 * Name: Flavio C. Palacios Tinoco                                                               *
 * Assignment: 03                                                                                *
 * Purpose: "TreeNode" struct and "BinaryTree" class declaration.                                *
 * Notes: Function prototypes are found here. The corresponding function implementations are seen*
 *        in the "binaryTree.cpp" file. Due to the nature of recursive functions, they are       *
 *        declared private functions to ensure certain pointers are not reset with every         *
 *        recursive iteration. "traverserPtr" for example. It points to the root before a        *
 *        recursive function is activated, and this ensures a starting value of "root" only      *
 *        once. Otherwise, if "traverstPtr" were included inside the recursive function, it would*
 *        always reset to "root" after a new recursive iteration is initiated.                   *
 * ***********************************************************************************************/

#ifndef BINARYTREE_H
#define BINARYTREE_H
#include <cstdlib>  //For using "random()".
#include <ctime>  //For using the system clock.
#include <cmath>  //For "pow()".
#include <queue>  //Helps print tree.
#include <string>  //For "to_sting()".
#include <iomanip>  //For "setw()".
#include <iostream>
using namespace std;

struct TreeNode{  //For node creations inside the "BinaryTree" class.
    int id;  //Node data. 
    int nodeLevel;  //Node level within tree.
    struct TreeNode *leftChild;
    struct TreeNode *rightChild;
    
    TreeNode(int,int);  //OVERLOADED CONSTRUCTOR.
};

class BinaryTree{  //Structure that creates and handles instances from "TreeNode".
private:
    int treeHeight;  //Possible value of 1-10.
    int nodeCount;  //Possible value of 1-10.
    struct TreeNode *root;  //Pointer identifying the tree.
    struct TreeNode *traverserPtr = root;  //Pointer used to move inside the tree.

    void addHelper(struct TreeNode *&, struct TreeNode *, int);  //(#1) Creates nodes. Recursive.
    void clearHelper(struct TreeNode *);  //(#2) Clears entire tree. Recursive.
    void preorderTraverseHelper(struct TreeNode *);  //(#3) Prints values. Recursive.
    void inorderTraverseHelper(struct TreeNode *);  //(#4) Prints values. Recursive.
    void postorderTraverseHelper(struct TreeNode *);  //(#5) Prints values. Recursive.
    void pseudoRandIntGenerator(int); //(#6) For node "id" generation. Non-Recursive.

public:
    BinaryTree();  //OVERLOADED CONSTRUCTOR

    bool isEmpty();  //Returns "true" if the tree exists.

    int getTreeHeight();  //Grabs "treeHeight" value.

    int getNumberOfNodes();  //Returns value of private variable "nodeCount".

    bool addNode(int);  //Calls private function (#6) which then calls recursive (#1).

    bool remove(int givenId);  //Deletes desired node. Returns "true" if successful.

    bool clear();  //Calls recursive private function (#2).

    bool contains(int);  //Serches desired node. Returns "true" if found.

    void preorderTraverse();  //Calls recursive private function (#3).

    void inorderTraverse();  //Calls recursive private function (#4).

    void postorderTraverse();  //Calls recursive private function (#5).

    void printLeavesAndEdges();  //Print tree.
};

#endif //BINARYTREE_H
