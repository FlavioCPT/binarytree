/* ***********************************************************************************************
 * Name: Flavio C. Palacios Tinoco                                                               *
 * Assignment: 03                                                                                *
 * Purpose: Main program                                                                         *
 * Notes: All abilities of the "BinaryTree" class are tested here. One command line argument must*
 *        be passed(a number 1-10) to run this program.                                          *
 *************************************************************************************************/
#include "main.h"
#include "functions.cpp"
#include "binaryTree.cpp"

int main(int argc,char **argv){
    bool appropriateArgs=false;  //"true" signifies valid command line argument.

    appropriateArgs=checkArgs(argc,argv[1]);  //"checkArgs()" validates user given command line argument.

    if(appropriateArgs==false){return 0;}  //"appropriateArgs" is "false". The program terminates.
    else{
        bool success;  //Bool used with "successEcho()". Helps print.
        int h;  //Holds tree hight.
        int n;  //Holds number of tree nodes.
        int pseudoRandNum; //Holds random number.
        BinaryTree Tree;  //"BinaryTree" instance creation. 

        //1st round of Testing: "BinaryTree" class instance is tested for conditions with no tree nodes exist. 
        cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
        cout<<setw(85)<<left<<"|                                TEST: The Empty Tree                               |"<<endl;
        cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
        success=Tree.isEmpty();
        h=Tree.getTreeHeight();
        n=Tree.getNumberOfNodes();
        cout<<"Empty Tree?: "<<successEcho(success)<<endl;
        cout<<"Tree Height: "<<h<<endl;
        cout<<"Node Count:  "<<n<<endl;
        cout<<"Pre Order Traversal: ";
        Tree.preorderTraverse();
        cout<<endl;
        cout<<"In Order Traversal:  ";
        Tree.inorderTraverse();
        cout<<endl;
        cout<<"Post Order Traversal:";
        Tree.postorderTraverse();
        cout<<endl;
        cout<<"Visual of Binary Tree: ";
        Tree.printLeavesAndEdges();
        cout<<endl<<endl;

        success=Tree.remove(999);
        cout<<"***ACTION 1: Remove non-existing ID value \"999\"."<<endl;
        cout<<"***Success should equal \"No\": "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        success=Tree.contains(854);
        cout<<"***ACTION 2: Search for non-existing ID value \"854\"."<<endl; 
        cout<<"***Success should equal \"No\": "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        success=Tree.clear();
        cout<<"***ACTION 3: Deleting an empty tree."<<endl;
        cout<<"***Success should equal \"No\": "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        //2nd round of Testing: "BinaryTree" class instance is tested for conditions with a number of tree nodes equal to given argument. 
        cout<<endl<<endl;
        cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
        cout<<setw(85)<<left<<"|                               TEST: The Filled Tree                               |"<<endl;
        cout<<setw(85)<<left<<"-------------------------------------------------------------------------------------"<<endl;
        success=Tree.addNode(atoi(argv[1]));
        cout<<"...adding nodes equal to given argument. Success should equal \"Yes\": "<<successEcho(success)<<endl;
        success=Tree.isEmpty();
        h=Tree.getTreeHeight();
        n=Tree.getNumberOfNodes();
        cout<<"Empty Tree?: "<<successEcho(success)<<endl;
        cout<<"Tree Height: "<<h<<endl;
        cout<<"Node Count:  "<<n<<endl;
        cout<<"Pre Order Traversal: ";
        Tree.preorderTraverse();
        cout<<endl;
        cout<<"In Order Traversal:  ";
        Tree.inorderTraverse();
        cout<<endl;
        cout<<"Post Order Traversal:";
        Tree.postorderTraverse();
        cout<<endl;
        cout<<"Visual of Binary Tree: ";
        Tree.printLeavesAndEdges();
        cout<<endl<<endl;

        success=Tree.contains(50);
        cout<<"***ACTION 1: Search for non-existing ID value \"50\"."<<endl;
        cout<<"***Success should equal \"No\": "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        //"pseudoRandNum" is used to help choose an existing node within the binary tree at random.
        success=false;
        while(success!=true){
            pseudoRandNum=(rand()%900)+100;
            success=Tree.contains(pseudoRandNum);
        };
            
        success=Tree.contains(pseudoRandNum);
        cout<<"***ACTION 2: Search for \""<<pseudoRandNum<<"\"."<<endl;
        cout<<"***Success?: "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        success=Tree.remove(pseudoRandNum);
        cout<<"***ACTION 3: Delete value \""<<pseudoRandNum<<"\"."<<endl;
        cout<<"***Success?: "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        success=Tree.contains(pseudoRandNum);
        cout<<"***ACTION 4: Delete non-existing ID value \""<<pseudoRandNum<<"\"."<<endl;
        cout<<"***Success should equal \"No\": "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

        //"pseudoRandNum" is used to help choose an existing node within the binary tree at random.
        success=false;
        while(success!=true){
            pseudoRandNum=(rand()%900)+100;
            success=Tree.contains(pseudoRandNum);
        };

        success=Tree.clear();
        cout<<"***ACTION 5: Delete entire tree."<<endl;
        cout<<"***Success?: "<<successEcho(success)<<endl<<endl;
        printTreeData(Tree);

    };

    return 0;
}