/* ***********************************************************************************************
 * Name: Flavio C. Palacios Tinoco                                                               *
 * Assignment: 03                                                                                *
 * Purpose: Main program header                                                                  *
 * Notes: Function prototypes from "function.cpp" are found here since "main.cpp" is to use      *
 *        functions from that file.                                                              *
 * ***********************************************************************************************/
#ifndef MAIN_H
#define MAIN_H
#include "binaryTree.h"
//"binaryTree.h" headers are shared here... 
// ---> #include <cstdlib>  //For using "random()".
// ---> #include <ctime>  //For using the system clock.
// ---> #include <cmath>  //For "pow()".
// ---> #include <queue>  //Helps print tree.
// ---> #include <string>  //For "to_sting()".
// ---> #include <iomanip>  //For "setw()".
// ---> #include <iostream>
// ---> using namespace std;
#include <cstring>  //For "strlen()".

bool checkArgs(int,char*);  //Checks command line for one integer argument of value "1" - "10".
string successEcho(bool);  //Value of returned string based on bool: "Yes" if "true".
void printTreeData(BinaryTree);  //Holds repetitive print statements.

#endif //MAIN_H
